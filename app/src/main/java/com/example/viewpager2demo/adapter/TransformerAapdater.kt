package com.example.viewpager2demo.adapter

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewpager2demo.fragment.DemoFragment

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/6
 * desc   :
 */
class TransformerAapdater : FragmentStateAdapter {

    private var fragments = mutableListOf<Fragment>()

    constructor(activity: FragmentActivity, fragmentList: List<Fragment>) : super(activity) {
        fragments.clear()
        fragments.addAll(fragmentList)
    }

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}