package com.example.viewpager2demo

import android.os.Bundle
import android.widget.RadioGroup
import android.widget.TableLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager2demo.adapter.DemoAapdater
import com.example.viewpager2demo.adapter.TransformerAapdater
import com.example.viewpager2demo.fragment.TransformerFragment
import com.example.viewpager2demo.transformer.*
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/7
 * desc   :
 */
class TransformerActivity : AppCompatActivity() {

    private lateinit var tableLayout: TabLayout
    private lateinit var viewPager2: ViewPager2
    private lateinit var transformerAapdater: TransformerAapdater

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transformer)
        initView()
    }

    private fun initView() {

        val fragments = mutableListOf<TransformerFragment>()
        val cascadeFragment = TransformerFragment("层叠", CascadeTransformer())
        fragments.add(cascadeFragment)
        val depthFragment = TransformerFragment("景深", DepthFieldTransformer())
        fragments.add(depthFragment)
        val flipFragment = TransformerFragment("翻转", FlipTransformer())
        fragments.add(flipFragment)
        val pushFragment = TransformerFragment("推压", PushTransformer())
        fragments.add(pushFragment)
        val rotationFragment = TransformerFragment("旋转", RotationTransformer())
        fragments.add(rotationFragment)
        val squareFragment = TransformerFragment("方块", SquareBoxTransformer())
        fragments.add(squareFragment)
        val windmillFragment = TransformerFragment("风车", WindMillTransformer())
        fragments.add(windmillFragment)

        tableLayout = findViewById(R.id.tabLayout)
        viewPager2 = findViewById(R.id.viewPager)
        viewPager2.isUserInputEnabled = false
        transformerAapdater = TransformerAapdater(this, fragments)
        viewPager2.adapter = transformerAapdater
        TabLayoutMediator(tableLayout, viewPager2) { tab, position ->
            tab.text = "${fragments[position].tabText}"
        }.attach()
    }
}