package com.example.viewpager2demo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager2demo.R
import com.example.viewpager2demo.adapter.DemoAapdater
import com.example.viewpager2demo.adapter.TransformerAapdater

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/6
 * desc   :
 */
class TransformerFragment : Fragment {

    constructor(text: String, transformer: ViewPager2.PageTransformer) : super() {
        tabText = text
        pageTransformer = transformer
    }

    var tabText = ""
    private lateinit var pageTransformer: ViewPager2.PageTransformer

    private lateinit var rootView: View
    private lateinit var viewPager2: ViewPager2
    private lateinit var demoAapdater: DemoAapdater

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_transformer, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager2 = rootView.findViewById(R.id.viewPager)
        demoAapdater = DemoAapdater(context as AppCompatActivity)
        viewPager2.adapter = demoAapdater
        viewPager2.setPageTransformer(pageTransformer)

    }
}