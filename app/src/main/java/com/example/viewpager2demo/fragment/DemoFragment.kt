package com.example.viewpager2demo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewpager2demo.R

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/6
 * desc   :
 */
const val TEXT = "TEXT"

class DemoFragment : Fragment() {

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_demo, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.takeIf { it.containsKey(TEXT) }?.apply {
            val textView = rootView.findViewById<TextView>(R.id.tvText)
            textView.text = getInt(TEXT).toString()
            textView.setBackgroundResource(PAGE_COLORS[getInt(TEXT) % PAGE_COLORS.size])
        }
    }
}

internal val PAGE_COLORS = listOf(
    android.R.color.holo_blue_light,
    android.R.color.holo_orange_light,
    android.R.color.holo_green_light,
    android.R.color.holo_red_light
)