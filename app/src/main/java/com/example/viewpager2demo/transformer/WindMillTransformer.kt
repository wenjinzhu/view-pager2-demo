package com.example.viewpager2demo.transformer

import android.view.View
import androidx.viewpager2.widget.ViewPager2

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/7
 * desc   : 风车
 */
class WindMillTransformer : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        page.apply {
            pivotX = width / 2f
            pivotY = height.toFloat()
            rotation = position * 90
        }
    }
}