package com.example.viewpager2demo.transformer

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/6
 * desc   : 翻转
 */
class FlipTransformer : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        page.apply {
            translationX = -width * position
            pivotX = width / 2f
            pivotY = height / 2f
            rotationY = 180 * position
            if (position > -0.5f && position < 0.5f) {
                visibility = View.VISIBLE;
            } else {
                visibility = View.INVISIBLE;
            }
            val absPos = abs(position)
            val scale = if (absPos > 1) 0F else 1 - absPos
            scaleX = scale
            scaleY = scale
        }
    }
}