package com.example.viewpager2demo.transformer

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/7
 * desc   : 层叠
 */
private const val MIN_SCALE = 0.85f

class CascadeTransformer : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        page.apply {
            when {
                position < -1 -> {
                    alpha = 0f
                    scaleX = 1.0f
                    scaleY = 1.0f
                }
                position <= 0 -> {
                    alpha = 1f
                    scaleX = 1.0f
                    scaleY = 1.0f
                    translationZ = 0f
                    translationX = 0f
                }
                position <= 1 -> {
                    alpha = 1 - position
                    translationX = -position * page.width
                    val scale = MIN_SCALE + (1 - MIN_SCALE) * (1 - abs(position))
                    scaleX = scale
                    scaleY = scale
                    translationZ = -position
                }
                else -> {
                    alpha = 0f
                }
            }
        }
    }
}