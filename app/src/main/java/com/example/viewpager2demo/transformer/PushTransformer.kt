package com.example.viewpager2demo.transformer

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/6
 * desc   : 推压
 */
class PushTransformer : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        page.apply {
            translationX = -position * width / 2
            val absPos = abs(position)
            val scale = if (absPos > 1) 0F else 1 - absPos
            scaleX = scale
        }
    }
}