package com.example.viewpager2demo.transformer

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/6
 * desc   : 景深
 */
private const val MIN_SCALE = 0.6f

class DepthFieldTransformer : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        page.apply {
            when {
                position < -1 -> {
                    alpha = 1f - MIN_SCALE
                    scaleX = MIN_SCALE
                    scaleY = MIN_SCALE
                }
                position <= 1 -> {
                    val absPos = abs(position)
                    val scale = if (absPos > 1) 0F else 1 - absPos
                    page.scaleX = MIN_SCALE + (1f - MIN_SCALE) * scale
                    page.scaleY = MIN_SCALE + (1f - MIN_SCALE) * scale
                    page.alpha = MIN_SCALE + (1f - MIN_SCALE) * scale
                }
                else -> {
                    page.scaleX = 0f
                    page.scaleY = 0f
                    page.alpha = 0f
                }
            }
        }


    }
}