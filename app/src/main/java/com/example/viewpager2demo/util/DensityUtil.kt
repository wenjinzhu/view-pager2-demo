package com.example.viewpager2demo.util

import android.content.Context

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/6
 * desc   :
 */
class DensityUtil {
    companion object {
        fun dp2px(context: Context, dpValue: Float): Int {
            return (dpValue * getDensity(context) + 0.5f).toInt()
        }

        private fun getDensity(context: Context): Float {
            return context.resources.displayMetrics.density
        }
    }
}