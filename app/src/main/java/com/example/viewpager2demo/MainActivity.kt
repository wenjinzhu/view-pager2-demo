package com.example.viewpager2demo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager2demo.adapter.DemoAapdater
import com.example.viewpager2demo.transformer.CascadeTransformer
import com.example.viewpager2demo.transformer.FlipTransformer
import com.example.viewpager2demo.transformer.RotationTransformer
import com.example.viewpager2demo.transformer.WindMillTransformer

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        val tvTablayout = findViewById<TextView>(R.id.tvTab)
        tvTablayout.setOnClickListener(this)
        val tvProperties = findViewById<TextView>(R.id.tvProperties)
        tvProperties.setOnClickListener(this)
        val tvTransformer = findViewById<TextView>(R.id.tvTransformer)
        tvTransformer.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvTab -> {
                startActivity(Intent(this, TablayoutActivity::class.java))
            }
            R.id.tvProperties -> {
                startActivity(Intent(this, PropertiesActivity::class.java))
            }
            R.id.tvTransformer -> {
                startActivity(Intent(this, TransformerActivity::class.java))
            }
        }
    }
}