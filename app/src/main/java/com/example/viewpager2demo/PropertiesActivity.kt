package com.example.viewpager2demo

import android.os.Bundle
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager2demo.adapter.DemoAapdater

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/7
 * desc   :
 */
class PropertiesActivity : AppCompatActivity() {

    private lateinit var viewPager2: ViewPager2
    private lateinit var demoAapdater: DemoAapdater
    private lateinit var cbDirection: CheckBox
    private lateinit var cbScroll: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_properties)
        initView()
    }

    private fun initView() {
        viewPager2 = findViewById(R.id.viewPager)
        demoAapdater = DemoAapdater(this)
        viewPager2.adapter = demoAapdater
        cbDirection = findViewById(R.id.cbDirection)
        cbDirection.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewPager2.orientation = ViewPager2.ORIENTATION_HORIZONTAL
            } else {
                viewPager2.orientation = ViewPager2.ORIENTATION_VERTICAL
            }
        }
        cbScroll = findViewById(R.id.cbScroll)
        cbScroll.setOnCheckedChangeListener { buttonView, isChecked ->
            viewPager2.isUserInputEnabled = isChecked
        }
    }
}