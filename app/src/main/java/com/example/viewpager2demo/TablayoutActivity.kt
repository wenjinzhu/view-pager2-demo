package com.example.viewpager2demo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager2demo.adapter.DemoAapdater
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * author : king_nina
 * e-mail : 13450414852@163.com
 * date   : 2021/12/7
 * desc   :
 */
class TablayoutActivity : AppCompatActivity() {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager2: ViewPager2
    private lateinit var demoAapdater: DemoAapdater

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tablayout)
        initView()
    }

    private fun initView() {
        tabLayout = findViewById(R.id.tabLayout)
        viewPager2 = findViewById(R.id.viewPager)
        demoAapdater = DemoAapdater(this)
        viewPager2.adapter = demoAapdater
        TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
            tab.text = "PAGE $position"
        }.attach()
    }
}